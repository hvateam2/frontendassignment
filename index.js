const getProductButton = document.getElementById('productButton');

getProductButton.addEventListener('click', function () {
    const getDisplayBox = document.getElementById('productTable');
    const createTable = document.createElement('table');
    createTable.id = 'productData';

    // Create table headers
    const headers = ["Brands", "Description", "Members", "Categories", "Tags", "Meetings", "+"];
    const Header = createTable.insertRow();
    headers.forEach(headerText => {
        const cell = Header.insertCell();
        cell.textContent = headerText;
    });

    // Data for 8 rows
    const rowData = [
        {   id:1,
            brand: { name: 'Wix', imgSrc: 'wix.png', description: 'Developed in personalized fit....' },
            members: ['gir.jpeg', 'boy1.jpeg', 'gir.jpeg', 'boy1.jpeg'],
            categories: ['Automation'],
            tags: ['#digitalTransformation', '#eCommerce'],
            meetings: ['in 30 mins']
        },
        {   id:2,
            brand: { name: 'Shopify', imgSrc: 'shop.jpeg', description: 'Introduce a cloud base p...' },
            members: ['gir.jpeg', 'boy1.jpeg', 'gir.jpeg', 'boy1.jpeg'],
            categories: ['e-Commerce', 'B2B'],
            tags: ['#cloudComputing', '#marketplace'],
            meetings: ['in 1 hour']
        },
        {   id:3,
            brand: { name: 'mailChimp', imgSrc: 'mailChamp.png', description: 'Develop a mobile app aim...' },
            members: ['gir.jpeg', 'boy1.jpeg', 'gir.jpeg', 'boy1.jpeg'],
            categories: ['SAAS', 'Mobile'],
            tags: ['#onLineShopping'],
            meetings: ['Tomorrow']
        },
        {   id:4,
            brand: { name: 'payPal', imgSrc: 'payPal.jpeg', description: 'This program could include...' },
            members: ['gir.jpeg', 'boy1.jpeg', 'gir.jpeg', 'boy1.jpeg','gir.jpeg'],
            categories: ['makrketPlace'],
            tags: ['Dailysellonline'],
            meetings: ['in 6 hours']
        },
        {   id:5,
            brand: { name: 'disney', imgSrc: 'disney.jpeg', description: 'Introduce  a B2B solution f..' },
            members: ['gir.jpeg', 'boy1.jpeg'],
            categories: ['B2B', 'B2C'],
            tags: ['#Bussinesspatnership'],
            meetings: ['not contact']
        },
        {   id:5,
            brand: { name: 'intercom', imgSrc: 'insta.png', description: 'Implement an AI driven..' },
            members: ['gir.jpeg', 'boy1.jpeg','gir.jpeg', 'boy1.jpeg','gir.jpeg', 'boy1.jpeg'],
            categories: ['Technology', 'SAAS'],
            tags: ['#smartFinance'],
            meetings: ['in 1 hour']
        },
        {
                id:6,      
                brand: { name: 'google', imgSrc: 'google.png', description: 'offers comprehensive cy...' },
                members: ['gir.jpeg', 'boy1.jpeg','gir.jpeg', 'boy1.jpeg','gir.jpeg', 'boy1.jpeg'],
                categories: ['finance', 'automation'],
                tags: ['#smartFinance'],
                meetings: ['in 30 minutes']
            
        },
        
            {    id:7,
                brand: { name: 'evernote', imgSrc: 'evernote.jpeg', description: 'This code included smart...' },
                members: ['gir.jpeg', 'boy1.jpeg','gir.jpeg'],
                categories: ['Transportation'],
                tags: ['#Logistics','#LIX'],
                meetings: ['next month']
            
        },
        {   id:8,
            brand: { name: 'microsoft', imgSrc: 'micro.png', description: 'Lauch an advisory service' },
                members: ['gir.jpeg', 'boy1.jpeg'],
                categories: ['Publishing', 'B2C'],
                tags: ['#B2CMarketing'],
                meetings: ['not contact']
        },
        {   id:9,
            brand: { name: 'invision', imgSrc: 'invioson.png', description: 'The tool word analyze tree..' },
                members: ['gir.jpeg', 'boy1.jpeg'],
                categories: ['webServices'],
                tags: ['#APIintegration'],
                meetings: ['next month']
        }
        

        // Add more rows data as needed...
    ];
    function applyStylesToRow(row) {
        const id = `#row-${row.id}`;
        let css = '';
        if (row.id === 1) {
            css = `
                ${id} input[type="checkbox"] {
                    margin-left:-100px;
                }
    
                ${id} img {
                    width:100px; /* Example custom width for row-1 */
                    height: 30px; /* Example custom height for row-1 */
                }
    
                /* Add more specific styles for row-1 as needed */
            `;}
        // Append the CSS rule to the document
        const style = document.createElement('style');
        style.appendChild(document.createTextNode(css));
        document.head.appendChild(style);
    }

    // Create rows
        rowData.forEach(data => {
            applyStylesToRow(data);
        
        
        const row = createTable.insertRow();

        // Brand cell
        const brandCell = row.insertCell();
        // Create a checkbox element
        const checkBox = document.createElement('input');
        checkBox.id="checkBoxStyle"
        checkBox.type = 'checkbox';
        brandCell.appendChild(checkBox);

        const brandImg = document.createElement('img');
        brandImg.src = data.brand.imgSrc;
        brandCell.appendChild(brandImg);
        brandCell.appendChild(document.createTextNode(data.brand.name));

        // Description cell
        const descriptionCell = row.insertCell();
        descriptionCell.textContent = data.brand.description;

        // Members cell
        const membersCell = row.insertCell();
        data.members.forEach(memberImgSrc => {
            const memberImg = document.createElement('img');
            memberImg.src = memberImgSrc;
            membersCell.appendChild(memberImg);
        });

        // Categories cell
        const categoriesCell = row.insertCell();
        data.categories.forEach(category => {
            const categoryBtn = document.createElement('button');
            categoryBtn.textContent = category;
            categoriesCell.appendChild(categoryBtn);
        });

        // Tags cell
        const tagsCell = row.insertCell();
        data.tags.forEach(tag => {
            const tagBtn = document.createElement('button');
            tagBtn.textContent = tag;
            tagsCell.appendChild(tagBtn);
        });

        // Meetings cell
        const meetingsCell = row.insertCell();
        data.meetings.forEach(meeting => {
            const meetingBtn = document.createElement('button');
            meetingBtn.textContent = meeting;
            meetingsCell.appendChild(meetingBtn);
        });

        // Add more cells as needed...
    });

    getDisplayBox.appendChild(createTable);
});
