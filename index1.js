document.addEventListener("DOMContentLoaded", function() {
    const productContainers = document.querySelectorAll('.product-Container');

    productContainers.forEach((container, index) => {
        const button = container.querySelector('.dropbtn');
        const dropdownContent = container.querySelector('.dropdown-content');

        button.addEventListener('click', function() {
            dropdownContent.classList.toggle('show');
            adjustContentPosition(dropdownContent.classList.contains('show'), container);
        });
    });

    function adjustContentPosition(productsVisible, container) {
        const contentBelow = container.nextElementSibling; // Select content below the container

        if (productsVisible) {
            contentBelow.style.marginTop = "200px"; // Adjust margin to shift content below products container
        } else {
            contentBelow.style.marginTop = ""; // Reset margin
        }
    }
});
